var fs = require('fs')

function imageToBase64(path) {
	let data = fs.readFileSync(path)

	return new Buffer(data).toString('base64')
}
