N=[0m
G=[01;32m
Y=[01;33m
B=[01;34m

comandos:
	@echo "${B}Los comandos disponibles son:${N} "
	@echo "    instalar_dependencias               Instala las dependencias necesarias para el proyecto"
	@echo "    electron                            Inicia electron con el proyecto"
	@echo "    electron_live                       Inicia electron en modo live"
	@echo "    compilar                            Compila la aplicacion para generar la pagina estatica"
	@echo "    empaquetar                          Empaqueta la pagina estatica con electron"
	@echo "    arreglo_policies                    Arregla el error de policies para sistemas linux"

copiar_libs:
ifeq ($(OS),Windows_NT)
	@cp node_modules\chosen-js\chosen-sprite.png static\css\chosen-sprite.png
	@cp node_modules\chosen-js\chosen.min.css static\css\chosen.min.css
	@cp node_modules\vue-notifyjs\themes\material.css static\css\notify.css
else
	@cp node_modules/chosen-js/chosen-sprite.png static/css/chosen-sprite.png
	@cp node_modules/chosen-js/chosen.min.css static/css/chosen.min.css
	@cp node_modules/vue-notifyjs/themes/default.css static/css/notify.css
endif

instalar_dependencias:
	@echo "${B}Instalando dependencias...${N}"
	@yarn install
	@echo "${B}Copiando librerias...${N}"
	@make copiar_libs > /dev/null
	@echo "${B}Arreglo de policies${N}"
	@make arreglo_policies

electron:
	@echo "${B}Iniciando electron...${N}"
	@yarn run start

electron_live:
	@echo "${B}Iniciando electron en tiempo real...${N}"
	@yarn run dev

compilar:
	@echo "${B}Compilando...${N}"
	@yarn run build

empaquetar:
	@echo "${B}Empaquetando...${N}"
	@yarn run pack

arreglo_policies:
ifeq ($(OS),Windows_NT)
	@echo "Ejecute el siguiente comando"
	@echo "parches\sed.exe -i \"s/const policies = /const policies = null \/\//\" node_modules\nuxt\lib\core\middleware\nuxt.js"
else
	@echo "${B}Aplicando parche...${N}"
	@sed -i 's/const policies = /const policies = null \/\//g' node_modules/nuxt/lib/core/middleware/nuxt.js
endif
