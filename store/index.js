import Vuex from 'vuex'

const createStore = () => {
    return new Vuex.Store({
        state: {
			dataModal: '',
            activeid: 0,
            cv: false,
            failed: false,
            aj: false,
            antPage: '/',
            user: '',
            admin: false,
            vueInstanceMainw: '',
			url: 'https://bax.snowlinks.net',
            urlImages: 'https://bax.snowlinks.net/images/',
			userVerificado: false,
        },
        mutations: {
            setAntPage(state, page) {
                state.antPage = page
            },
            setUser(state, user) {
                state.user = user
            },
            setAdmin(state, admin) {
                state.admin = admin
            },
            mostrarAJ(state) {
                state.aj = true
            },
            mostrarCV(state) {
                state.cv = true
            },
            mostrarFailed(state) {
                state.failed = true
            },
            allFalse(state) {
                state.cv = false
                state.aj = false
                state.failed = false
            },
			setDataModal(state, data) {
				state.dataModal = data
			},
            setVueInstanceMainw(state, instance) {
                state.vueInstanceMainw = instance
            },
            setActiveID(state, id) {
                state.activeid = id
            },
            setVerificado(state, verificado) {
                state.userVerificado = verificado
            },
        }
    })
}

export default createStore
