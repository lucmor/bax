const webpack = require('webpack')

module.exports = {
    build: {
        loading: {
            Color: 'red',
            height: '3px'
        },
        extend(config, { isClient }) {
            // Extend only webpack config for client-bundle
            if (isClient) {
                config.target = 'electron-renderer'
            }
        },
        plugins: [
          new webpack.ProvidePlugin({
            'jQuery': 'jquery',
          })
        ],
        vendor: ['axios', 'jquery']
    },
    electron: {},
    css: [
      '~/static/css/standard.css'
    ],
    head: {
		script: [
			{  }
		],
        link: [
            { rel: 'stylesheet', href: 'https://meyerweb.com/eric/tools/css/reset/reset.css' },
            { rel: 'stylesheet', href: '/css/chosen.min.css' }
        ]
    }
}
